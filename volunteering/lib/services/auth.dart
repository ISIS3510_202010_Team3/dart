import 'package:firebase_auth/firebase_auth.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/database.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // crea user object basado en firebase user
  User _userFromFirebaseUser(FirebaseUser user){
    return user != null ? User(uid: user.uid) : null;
  }

  //Cambio en la autenticacion stream
  Stream <User> get user{
    return _auth.onAuthStateChanged
      .map(_userFromFirebaseUser);
  }

  //sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;

      return _userFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // register with email & password
  Future registerWithEmailAndPassword(String email, String password, String firstName, String lastName, String birthDate, String userName, String description, String picture, List<String> activities, List<String> interested, List<String> categories, List<dynamic> reviews) async {
    try{
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      print(birthDate);
      print(password);
      print(firstName);
      print("foto : $picture");
      print(activities);
      print(interested);
      print(reviews);
       //crea un nuevo documento con el uid del usurario
      await DatabaseService(uid: user.uid).updateUserData(birthDate, description, firstName, lastName, userName,picture,activities, interested, categories, reviews);

      return _userFromFirebaseUser(user);
      
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  //sign out
  Future signOut() async {
    try{
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;

    }
  }
}