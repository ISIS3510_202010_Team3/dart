import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:volunteering/models/user.dart';



class DatabaseService {

  final String uid;
  DatabaseService({this.uid});

//collection reference
final CollectionReference userCollection = Firestore.instance.collection('users');
final CollectionReference activityCollection = Firestore.instance.collection('activities');

Future updateUserData(String birthDate, String description, String firstName, String lastName, String username, String picture, List<String> activities, List<String> interested, List<String> categories, List<dynamic> reviews) async {
  return await userCollection.document(uid).setData({
    'birthDate': birthDate,
    'description': description,
    'firstName': firstName,
    'lastName': lastName,
    'username': username,
    'picture': picture,
    'activities': activities,
    'interested': interested,
    'categories': categories,
    'reviews':reviews,
  });   
}

//user data from snapshot
UserData _userDataFromSnapshot(DocumentSnapshot snapshot){
  return UserData(
    uid: uid,
    birthDate: snapshot.data['birthDate'],
    description: snapshot.data['description'],
    firstName: snapshot.data['firstName'],
    lastName: snapshot.data['lastName'],
    username: snapshot.data['username'],
    picture: snapshot.data['picture'],
    activities: snapshot.data['activities'].cast<String>(),
    interested: snapshot.data['interested'].cast<String>(), 
    categories: snapshot.data['categories'].cast<String>(),
    reviews: snapshot.data['reviews'].cast<dynamic>(),
  );
}


 //get user doc stream
Stream<UserData> get userdata{
  print(userCollection.document(uid).snapshots());
  return userCollection.document(uid).snapshots()
    .map(_userDataFromSnapshot);
} 


}