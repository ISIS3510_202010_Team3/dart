import 'dart:convert' show jsonDecode, jsonEncode;


import 'package:volunteering/models/activitie.dart';
import 'package:http/http.dart';

class HttpService{
  final String activitiesUrl="http://3.228.168.162:3000/activities/";
  final String photosUrl="http://3.228.168.162:3000/photos/image/";

  Future<List<Activitie>> getActivities() async{
    Response res = await get(activitiesUrl);
      if (res.statusCode == 200) {
        List<dynamic> data= jsonDecode(res.body);
        List<Activitie> actividad = data
           .map((dynamic item) => Activitie.fromJson(item))
           .toList();  
     return actividad;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception("Se murio esto");
  }
  }

  Future<List<Imagen>> getImage(String activityId) async{
    Response res = await get(photosUrl+activityId);
      if (res.statusCode == 200) {
        List<dynamic> data= jsonDecode(res.body);
        print(data);
        List<Imagen> imagen = data
           .map((dynamic item) => Imagen.fromJson(item))
           .toList();  
        print(imagen);
     return imagen;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception("Se murio esto");
  }
  
  }

  Future<Response> updateActivity(String activityId, Activitie item) async{
    
    Map<String, String> headers={"Content-type": "application/json"};
    print(item.volunteers);
    print(activityId);
    Response res= await put(activitiesUrl+activityId,headers: headers, body: jsonEncode(item.toJson()));
    print('llego aca');
    print(res);
    if(res.statusCode==200){
      return res;
    }
    else{
      throw Exception('Failed to update');
    }
  }

  /*Future<Response> deleteAlbum(String id) async {
  final Response response = await delete(
    activitiesUrl+id,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );
  if(response.statusCode==200){
    return response;
  }
  else{
    throw Exception('Failed to delete activity.');
  }
  
  
}*/

}