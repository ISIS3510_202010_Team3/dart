
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/models/activitie.dart';
import 'package:volunteering/screens/home/activitie_detail.dart';
import 'package:volunteering/screens/home/profilefin.dart';
import 'package:volunteering/services/http.dart';
import 'package:volunteering/screens/home/map.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/database.dart';



class Activities2 extends StatefulWidget {
  @override
  _Activities2State createState() => _Activities2State();
}

class _Activities2State extends State<Activities2> {
  final HttpService httpService = HttpService();
  
  Position position;
  List<Placemark> placemark;


  
  

  @override
  Widget build(BuildContext context) {
    var connectionStatus = Provider.of<ConnectivityStatus>(context);
    
    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
            stream: DatabaseService(uid: user.uid).userdata,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                UserData userData = snapshot.data;
                return ListView(
      children: <Widget>[
        Container(
          width: 450,
          height: 120,
          child: FlatButton(
            onPressed: () =>
               Navigator.of(context)
              .push(MaterialPageRoute(
                builder: (context) =>
                Map(),)),
                  color: Colors.green[300],
                  child: Text(
                  'View activities in the map',
                style: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
          ),
          )),
        ),
        SizedBox(
          height: 500,
          child: Container(
              margin: EdgeInsets.all(5.0),
              child: FutureBuilder(
                future: httpService.getActivities(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Activitie>> snapshot) {    
                  if (connectionStatus == ConnectivityStatus.WiFi ||
                      connectionStatus == ConnectivityStatus.Cellular) {
                    print(snapshot);
                    print(ConnectivityStatus);
                    if (snapshot.hasData) {
                      List<Activitie> actividades = snapshot.data;
                      List<String> categ= userData.categories;
                      List<Activitie> activitiesC=[];
                      for(var i in actividades){
                        if(categ.contains(i.category)){
                          activitiesC.add(i);
                        }
                      }
                      print(actividades);
                      return ListView(
                        children: activitiesC
                            .map((Activitie actividad) => Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin:
                                              EdgeInsets.fromLTRB(15, 5, 0, 10),
                                          child: Text(actividad.name,
                                              style: TextStyle(
                                                fontSize: 23.0,
                                                fontFamily: 'Roboto',
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            /*Container(
                            width:120,
                        height:120,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(actividad.images[0].fileName),
                          ),
                        ),),*/
                                            Expanded(
                                              child:
                                                  Text(actividad.description),
                                            ),
                                          ],
                                        ),
                                        Row(children: <Widget>[
                                          Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  5, 0, 0, 7),
                                              child: Icon(
                                                Icons.location_on,
                                                color: Colors.grey,
                                                size: 25,
                                              )),
                                          Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  2, 0, 180, 5),
                                              child: Text('Usaquén',
                                                  style: TextStyle(
                                                    fontFamily: 'Roboto',
                                                    fontSize: 16,
                                                  ))),
                                          FlatButton(
                                              onPressed: () =>
                                                  Navigator.of(context)
                                                      .push(MaterialPageRoute(
                                                    builder: (context) =>
                                                        ActivitieDetail(
                                                      activitie: actividad,
                                                    ),
                                                  )),
                                              color: Colors.green[300],
                                              child: Text(
                                                'Learn more',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Roboto',
                                                ),
                                              )),
                                        ]
                                        ),
                                      ]),
                                ))
                            .toList(),
                      );
                    }
                    return Center(child: CircularProgressIndicator());
                  }
                  if (connectionStatus == ConnectivityStatus.Offline) {
                    print("no hay conexion");
                    return AlertDialog(
                      title: Text("No internet"),
                      content: Text("You are not connected to a network"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Ok"),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileFin()));
                          },
                        )
                      ],
                    );
                  }
                  return Container();
                },
              )),
        )
      ],
    );}
    return Container();});
  }
}
