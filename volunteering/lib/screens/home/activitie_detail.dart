import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:volunteering/models/activitie.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/screens/home/home.dart';
import 'package:volunteering/services/database.dart';
import 'package:volunteering/services/http.dart';
import 'package:volunteering/screens/home/useractivities.dart';



class ActivitieDetail extends StatelessWidget {
  
  final Activitie activitie;
  final HttpService http= HttpService();

  ActivitieDetail({@required this.activitie});

  
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    var enable=true;
    var enable2=true;
    var gente;

    Future navigateToYourActivities(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => UserActivities()));
    }

    Future navigateToActivities (context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }

    Future ayuda(String us) async{
      Firestore.instance.collection('users').where('uid', isEqualTo: us).getDocuments()
      .then((QuerySnapshot docs){
        print("NOOOO ENTRAAAAA");
        print(us);
        print(docs.documents.length);
        print(Firestore.instance.collection('users').where('uid', isEqualTo: us).getDocuments());
        if(docs.documents.length!=0){
          print("ACAAAAAAAAAAAAAAA");
          gente= docs.documents[0].data["firstName"];
        }

      });
      return gente;
    }

    Future getContents1(String path) async { 
      var firestore = Firestore.instance; 
      QuerySnapshot qn = await firestore.collection("users").where('document_id', isEqualTo: path).getDocuments(); 
      print("HELPSOOOOOOSS");
      print(firestore.collection("users").where('uid', isEqualTo: path).toString());
      print(qn.documents[0].data['firstName']);
      return qn.documents[0].data['firstName']; 
    }

        return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userdata,
      builder: (context, snapshot) {
        if(snapshot.hasData){ 
          UserData userData = snapshot.data;
          if(userData.activities.contains(activitie.id)){
            enable=false;
          }
          if(userData.interested.contains(activitie.id)){
            enable2=false;
          }
          return Scaffold(
          
          appBar: AppBar(
            title: Text('Activity'),
            backgroundColor: Colors.green,
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Card(
                
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: Text("Name"),
                      subtitle: Text(activitie.name),
                    ),
                    ListTile(
                      title: Text("Description"),
                      subtitle: Text(activitie.description),
                    ),
                ListTile(
                  title: Text("Date"),
                  subtitle: Text(activitie.date),
                ),
                ListTile(
                  title: Text("Category",),
                  subtitle: Text(activitie.category),
                ),
               
                ListTile(
                  title: Text("Details"),
                  subtitle: Text("            Volunteers Needed                 Spots Left", textAlign: TextAlign.justify,),
                
                ),
                
                Row(children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(70, 0, 65, 10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.green, width: 5),
                      shape: BoxShape.circle,
                      
                      //color: Colors.green,
                    ),
                    child: 
                      Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Text(
                          activitie.volunteersNeed.toString(),
                          style: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                          ),
                        ),
                      ),
                    ),
                    Container(
                      //margin: EdgeInsets.fromLTRB(20, 25, 20, 0),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.green, width: 5),
                        shape: BoxShape.circle,
                        
                        //color: Colors.green,
                      ),
                      child: 
                        Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Text(
                            (activitie.volunteersNeed-activitie.volunteersAttending).toString(),
                            style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Roboto',
                            ),
                          ),
                        ),
                    ),
                ]),
                
                Row(children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(70, 0, 60, 10),
                  child: RaisedButton(
                    color:  Colors.green,
                    child: Text(
                      'Interested',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: !enable2? null:() async{
                      userData.interested.add(activitie.id);
                        await DatabaseService(uid: user.uid).updateUserData(
                          userData.birthDate,
                          userData.description,
                          userData.firstName,
                          userData.lastName,
                          userData.username,
                          userData.picture,
                          userData.activities,
                          userData.interested,
                          userData.categories,
                          userData.reviews,
                          );
                          navigateToActivities(context);
                    },),),
                    
                    Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child:
                    RaisedButton(
                    color:  Colors.green,
                    child: Text(
                      'Subscribe',
                      style: TextStyle(color: Colors.white),
                    ),
                    
                    onPressed: !enable? null:() async{
                      userData.activities.add(activitie.id);

                        await DatabaseService(uid: user.uid).updateUserData(
                          userData.birthDate,
                          userData.description,
                          userData.firstName,
                          userData.lastName,
                          userData.username,
                          userData.picture,
                          userData.activities,
                          userData.interested,
                          userData.categories,
                          userData.reviews,
                        );
                        List<String> vol= activitie.volunteers;
                        print(vol);
                        vol.add(user.uid);
                        print(vol);
                        final act= Activitie(
                          volunteers: vol,
                          id: activitie.id,
                          name: activitie.name,
                          description: activitie.description,
                          volunteersNeed: activitie.volunteersNeed,
                          volunteersAttending: activitie.volunteersAttending+1,
                          date: activitie.date,
                          images: activitie.images,
                          category: activitie.category,
                          location: activitie.location
                        );
                        http.updateActivity(activitie.id, act);
                        navigateToActivities(context); 
                        
                    },
                  ),)
                ],),
                ListTile(
                  title: Text("Users attending:",),
                ),
                Flexible(
                 child: ListView.builder(
                   shrinkWrap: true,
                   physics: NeverScrollableScrollPhysics(),
                   itemCount: activitie.volunteers.length,
                   itemBuilder: (context, index){
                     return StreamBuilder<DocumentSnapshot>(
                       stream: Firestore.instance.collection("users").document(activitie.volunteers[index]).snapshots(),
                       builder: (context, snapshot){
                         if(!snapshot.hasData){
                           return 
                                  Text( "Loading");
                                  //Text( act[0].score)
                         }
                         else{
                         List<dynamic> act=snapshot.data['reviews'];
                         int puntaje=0;
                         double promedio=0;
                         print("SCOOOOREEEE");
                         if(act!=null){
                           for(int i=0; i<act.length;i++){
                              Map mapeo= act[i];
                              puntaje+=int.parse(mapeo['score']);
                              print(int.parse(mapeo['score']));
                           }
                           promedio=(puntaje/act.length);
                         }
                         
                         print("PLEASEEEE");
                         print(puntaje);
                         print(promedio);
                           return Row(
                                children: <Widget>[
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 10, 0),
                                  child: Icon(
                                    Icons.person,
                                    size: 35,
                                  )
                                ),
                                Column(children: <Widget>[
                                  Text( snapshot.data['firstName']+" "+snapshot.data['lastName']),
                                  Text( promedio.toString())
                                ],)
                             
                           ],);
                         }
                       }
                     );
                   })
                  //children: activitie.volunteers.map((String usuario)=>
                   /*Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0))),
                          child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Icon(
                        Icons.person,
                        size: 35,
                      )
                    ),*/
                  
                  

                  
                    /*])
                  )*/
                
                )],),
             ),
            ),
         )
       );
     }
     return Container();
   }
        );
        }
        }