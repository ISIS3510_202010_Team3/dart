
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/models/activitie.dart';
import 'package:volunteering/screens/home/activitie_detail.dart';
import 'package:volunteering/screens/home/profilefin.dart';
import 'package:volunteering/services/http.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/database.dart';

class UserActivities extends StatefulWidget {
  @override
  _UserActivities createState() => _UserActivities();
}

class _UserActivities extends State<UserActivities> {
  final HttpService httpService = HttpService();

  @override
  Widget build(BuildContext context) {
        var connectionStatus = Provider.of<ConnectivityStatus>(context);

    final user = Provider.of<User>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text('Your Activities'),
          backgroundColor: Colors.green,
        ),
        body: StreamBuilder<UserData>(
            stream: DatabaseService(uid: user.uid).userdata,
            builder: (context, snapshot) {
               if (connectionStatus == ConnectivityStatus.WiFi ||
                      connectionStatus == ConnectivityStatus.Cellular) {
              if (snapshot.hasData) {
                UserData userData = snapshot.data;
                return ListView(children: <Widget>[
                  SizedBox(
                      height: 500,
                      child: Container(
                          margin: EdgeInsets.all(5.0),
                          child: FutureBuilder(
                              future: httpService.getActivities(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<List<Activitie>> snapshot) {
                                print(snapshot);
                                if (snapshot.hasData) {
                                  List<Activitie> actividades = snapshot.data;
                                  List<Activitie> interesado = [];
                                  List<Activitie> subscribed = [];
                                  print(actividades);
                                  for (var i in actividades) {
                                    if (userData.interested.contains(i.id)) {
                                      print(i.id);
                                      print(i);
                                      interesado.add(i);
                                    }
                                  }
                                  for (var i in actividades) {
                                    if (userData.activities.contains(i.id)) {
                                      subscribed.add(i);
                                    }
                                  }
                                  return Column(
                                    children: <Widget>[
                                      Container(
                                        child: Align(
                                          alignment: Alignment.topCenter,
                                          child: Text(
                                              "Activities you are interested in:",
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Roboto',
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListView(
                                          children: interesado
                                              .map(
                                                  (Activitie actividad) => Card(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius.all(
                                                                    Radius.circular(
                                                                        10.0))),
                                                        child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .fromLTRB(
                                                                        15,
                                                                        5,
                                                                        0,
                                                                        10),
                                                                child: Text(
                                                                    actividad
                                                                        .name,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          23.0,
                                                                      fontFamily:
                                                                          'Roboto',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                              Row(
                                                                children: <
                                                                    Widget>[
                                                                  /*Container(
                            width:120,
                        height:120,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(actividad.images[0].fileName),
                          ),
                        ),),*/
                                                                  Expanded(
                                                                    child: Text(
                                                                        actividad
                                                                            .description),
                                                                  ),
                                                                ],
                                                              ),
                                                              Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                        margin: EdgeInsets.fromLTRB(
                                                                            5,
                                                                            0,
                                                                            0,
                                                                            7),
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .location_on,
                                                                          color:
                                                                              Colors.grey,
                                                                          size:
                                                                              25,
                                                                        )),
                                                                    Container(
                                                                        margin: EdgeInsets.fromLTRB(
                                                                            2,
                                                                            0,
                                                                            140,
                                                                            5),
                                                                        child: Text(
                                                                            'Usaquén',
                                                                            style:
                                                                                TextStyle(
                                                                              fontFamily: 'Roboto',
                                                                              fontSize: 16,
                                                                            ))),
                                                                    FlatButton(
                                                                        onPressed: () =>
                                                                            Navigator.of(context).push(
                                                                                MaterialPageRoute(
                                                                              builder: (context) => ActivitieDetail(
                                                                                activitie: actividad,
                                                                              ),
                                                                            )),
                                                                        color: Colors.green[
                                                                            300],
                                                                        child:
                                                                            Text(
                                                                          'Learn more',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                15.0,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            fontFamily:
                                                                                'Roboto',
                                                                          ),
                                                                        )),
                                                                        IconButton(
                                                                          icon: Icon(Icons.delete,
                                                                          color: Colors.red,),
                                                                          onPressed: () async{
                                                                            userData.interested.remove(actividad.id);
                                                                            await DatabaseService(uid: user.uid).updateUserData(
                                                                              userData.birthDate,
                                                                              userData.description,
                                                                              userData.firstName,
                                                                              userData.lastName,
                                                                              userData.username,
                                                                              userData.picture,
                                                                              userData.activities,
                                                                              userData.interested,
                                                                              userData.categories,
                                                                              userData.reviews,
                                                                              );
                                                                            List<String> vol= actividad.volunteers;
                                                                            print(vol);
                                                                            vol.remove(user.uid);
                                                                            print(vol);
                                                                            final act= Activitie(
                                                                              volunteers: vol,
                                                                              id: actividad.id,
                                                                              name: actividad.name,
                                                                              description: actividad.description,
                                                                              volunteersNeed: actividad.volunteersNeed,
                                                                              volunteersAttending: actividad.volunteersAttending-1,
                                                                              date: actividad.date,
                                                                              images: actividad.images,
                                                                              category: actividad.category,
                                                                              location: actividad.location
                                                                            );
                                                                            httpService.updateActivity(actividad.id, act);
                                                                          },
                                                                        ),
                                                                  ]),
                                                            ]),
                                                      ))
                                              .toList(),
                                        ),
                                      ),
                                      Container(
                                        child: Align(
                                          alignment: Alignment.topCenter,
                                          child: Text(
                                              "Activities you are subscribed to:",
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Roboto',
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListView(
                                          children: subscribed
                                              .map(
                                                  (Activitie actividad) => Card(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius.all(
                                                                    Radius.circular(
                                                                        10.0))),
                                                        child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .fromLTRB(
                                                                        15,
                                                                        5,
                                                                        0,
                                                                        10),
                                                                child: Text(
                                                                    actividad
                                                                        .name,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          23.0,
                                                                      fontFamily:
                                                                          'Roboto',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                              Row(
                                                                children: <
                                                                    Widget>[
                                                                  /*Container(
                            width:120,
                        height:120,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(actividad.images[0].fileName),
                          ),
                        ),),*/
                                                                  Expanded(
                                                                    child: Text(
                                                                        actividad
                                                                            .description),
                                                                  ),
                                                                ],
                                                              ),
                                                              Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                        margin: EdgeInsets.fromLTRB(
                                                                            5,
                                                                            0,
                                                                            0,
                                                                            7),
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .location_on,
                                                                          color:
                                                                              Colors.grey,
                                                                          size:
                                                                              25,
                                                                        )),
                                                                    Container(
                                                                        margin: EdgeInsets.fromLTRB(
                                                                            2,
                                                                            0,
                                                                            140,
                                                                            5),
                                                                        child: Text(
                                                                            'Usaquén',
                                                                            style:
                                                                                TextStyle(
                                                                              fontFamily: 'Roboto',
                                                                              fontSize: 16,
                                                                            ))),
                                                                    FlatButton(
                                                                        onPressed: () =>
                                                                            Navigator.of(context).push(
                                                                                MaterialPageRoute(
                                                                              builder: (context) => ActivitieDetail(
                                                                                activitie: actividad,
                                                                              ),
                                                                            )),
                                                                        color: Colors.green[
                                                                            300],
                                                                        child:
                                                                            Text(
                                                                          'Learn more',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                15.0,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            fontFamily:
                                                                                'Roboto',
                                                                          ),
                                                                        )),
                                                                        IconButton(
                                                                          icon: Icon(Icons.delete,
                                                                          color: Colors.red,),
                                                                          onPressed: () async{
                                                                            userData.activities.remove(actividad.id);
                                                                            await DatabaseService(uid: user.uid).updateUserData(
                                                                              userData.birthDate,
                                                                              userData.description,
                                                                              userData.firstName,
                                                                              userData.lastName,
                                                                              userData.username,
                                                                              userData.picture,
                                                                              userData.activities,
                                                                              userData.interested,
                                                                              userData.categories,
                                                                              userData.reviews,
                                                                            );
                                                                            List<String> vol= actividad.volunteers;
                                                                            print(vol);
                                                                            vol.remove(user.uid);
                                                                            print(vol);
                                                                            final act= Activitie(
                                                                              volunteers: vol,
                                                                              id: actividad.id,
                                                                              name: actividad.name,
                                                                              description: actividad.description,
                                                                              volunteersNeed: actividad.volunteersNeed,
                                                                              volunteersAttending: actividad.volunteersAttending,
                                                                              date: actividad.date,
                                                                              images: actividad.images,
                                                                              category: actividad.category,
                                                                              location: actividad.location
                                                                            );
                                                                            httpService.updateActivity(actividad.id, act);
                                                                          },
                                                                        ),
                                                                  ]),
                                                            ]),
                                                      ))
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  );
                                }
                                else{
                                  return Container();
                                }
                              })))
                ]);
              }
              else {
                return Container();
              }
                      }
                      if (connectionStatus == ConnectivityStatus.Offline) {
                    print("no hay conexion");
                    return AlertDialog(
                      title: Text("No internet"),
                      content: Text("You are not connected to a network"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Ok"),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileFin()));
                          },
                        )
                      ],
                    );
                  }
                  return Container();
            }));
  }
}
