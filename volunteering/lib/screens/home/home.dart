import 'package:flutter/material.dart';
import 'package:volunteering/screens/home/activities2.dart';


import 'package:volunteering/screens/home/useractivities.dart';
import 'package:volunteering/services/auth.dart';
import 'package:volunteering/screens/home/profilefin.dart';
import 'package:volunteering/screens/home/map.dart';


class Home extends StatelessWidget{
  
  final AuthService _auth = AuthService();
  
  @override
  Widget build(BuildContext context){

 //void _showSettingsPanel(){
 //     showModalBottomSheet(context: context, builder: (context){
 //       return Container(
 //         padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
 //         child: SettingsForm(),
 //       );
 //     });
 //   }

    
    Future navigateToProfile(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileFin()));
    }
    
    Future navigateToMyActivities(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => UserActivities()));
    }
    Future navigateToMap(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Map()));
    }

    return  Scaffold(
            backgroundColor: Colors.white,
            appBar:  AppBar(
            title: Text('VolunteeringHub'),
            backgroundColor: Colors.green,
            elevation: 0.0,
            actions: <Widget>[
              
              IconButton(
                icon: Icon(Icons.person),
                onPressed: () {
                  navigateToProfile(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.pin_drop),
                onPressed: () {
                  navigateToMap(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.subscriptions),
                onPressed: () {
                  navigateToMyActivities(context);
                },
              ),
              FlatButton.icon(
                icon: Icon(Icons.exit_to_app),
                label: Text('logout'),
                onPressed: () async {
                  await _auth.signOut();
                },
              ),
            ],
          ),
          body: Activities2(),
     );
  }
}