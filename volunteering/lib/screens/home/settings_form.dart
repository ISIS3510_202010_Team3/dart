import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/database.dart';


class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  
  final _formKey = GlobalKey<FormState>();

  String _currentFirstName;
  String _currentLastName;
  String _currentUserName;
  String _currentBirthDate;
  String _currentDescription;
  String _currentPicture;
  List<String> _activities;
  List<String> _interested;
  List<String> categories= List<String>();
  List<Review> _reviews;


 bool isSwitchedEnvironment = false;
 bool isSwitchedVulnerablePopulations = false;
 bool isSwitchedHandicapPeople = false;
 bool isSwitchedAnimals = false;
 bool isSwitchedTutoring = false;

 String environment="environment";
  String tutoring="tutoring";
 String elder="elder";
  String animals="animals";
  String vulnerable="vulnerable";

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    return Scaffold(
          appBar: AppBar(title: Text('VolunteeringHub'),
            backgroundColor: Colors.green,),
          body:  StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userdata,
        builder: (context, snapshot) {
          if(snapshot.hasData){ 
            UserData userData = snapshot.data;
            return Form(
            key: _formKey,
            child: ListView(
             children: <Widget>[Column(
              children: <Widget>[
                SizedBox(height: 25.0),
                 Container(
                child:Align(
                alignment: Alignment.topCenter,
                  child: Text(
                    ' Update Profile information',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
                SizedBox(height: 25.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "First name",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0) ,
                      borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0)) 
                  ),
                  initialValue: userData.firstName,
                  validator: (val) => val.isEmpty  ? 'Please enter first name' : null,
                  onChanged: (val) => setState(() => _currentFirstName = val),
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Last name",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0) ,
                      borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0)) 
                  ),
                  initialValue: userData.lastName,
                  validator: (val) => val.isEmpty  ? 'Please enter last name' : null,
                  onChanged: (val) => setState(() => _currentLastName = val),
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "User name",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0) ,
                      borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0)) 
                  ),
                  initialValue: userData.username,
                  validator: (val) => val.isEmpty  ? 'Please enter user name' : null,
                  onChanged: (val) => setState(() => _currentUserName = val),
                ),
                SizedBox(height: 10.0),
                SizedBox(height: 10.0),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Enter your description",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0) ,
                      borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0)) 
                  ),
                  initialValue: userData.description,
                  validator: (val) => val.isEmpty  ? 'Please enter Description' : null,
                  onChanged: (val) => setState(() => _currentDescription = val),
                ),
                Text("Update the categories you are interested in", textAlign: TextAlign.left,),
                Row(children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 120, 0),
                  child: Text("Environment"),
                  ),
                Container(
                  child:  Switch(
                      value: isSwitchedEnvironment,
                      onChanged: (value){
                           setState(() {
                           isSwitchedEnvironment=value;
                           if(value==true){
                             categories.add(environment);
                           }
                           if(value==false){
                             categories.remove(environment);
                           }
                           print(isSwitchedEnvironment);
                           print(categories);
                      });
                      },
                      activeTrackColor: Colors.lightGreenAccent,
                      activeColor: Colors.green,
                        ),
                    ),
                  ],),
                  Row(children: <Widget>[
                    Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 60, 0),
                  child: Text("Vulnerable populations"),
                  ),
                Container(
                  child:  Switch(
                      value: isSwitchedVulnerablePopulations,
                      onChanged: (value){
                           setState(() {
                           isSwitchedVulnerablePopulations=value;
                           if(value==true){
                             categories.add(vulnerable);
                           }
                           if(value==false){
                             categories.remove(vulnerable);
                           }
                           print(isSwitchedVulnerablePopulations);
                           print(categories);
                           print(isSwitchedVulnerablePopulations);
                      });
                      },
                      activeTrackColor: Colors.lightGreenAccent,
                      activeColor: Colors.green,
                        ),
                    ),
                  ]),
                  Row(children: <Widget>[
                    Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 50, 0),
                  child: Text("Helping handicap people"),
                  ),
                  Container(
                  child:  Switch(
                      value: isSwitchedHandicapPeople,
                      onChanged: (value){
                           setState(() {
                           isSwitchedHandicapPeople=value;
                           if(value==true){
                             categories.add(elder);
                           }
                           if(value==false){
                             categories.remove(elder);
                           }
                           print(isSwitchedHandicapPeople);
                           print(categories);
                           print(isSwitchedHandicapPeople);
                      });
                      },
                      activeTrackColor: Colors.lightGreenAccent,
                      activeColor: Colors.green,
                        ),
                    ),
                  ]),
                  Row(children: <Widget>[
                    Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 155, 0),
                  child: Text("Animals"),
                  ),      
                  Container(
                  child:  Switch(
                      value: isSwitchedAnimals,
                      onChanged: (value){
                           setState(() {
                           isSwitchedAnimals=value;
                           if(value==true){
                             categories.add(animals);
                           }
                           if(value==false){
                             categories.remove(animals);
                           }
                           print(isSwitchedAnimals);
                           print(categories);
                           print(isSwitchedAnimals);
                      });
                      },
                      activeTrackColor: Colors.lightGreenAccent,
                      activeColor: Colors.green,
                        ),
                    ),
                  ]),
                  Row(children: <Widget>[
                    Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 155, 0),
                  child: Text("Tutoring"),
                  ),
                  Container(
                  child:  Switch(
                      value: isSwitchedTutoring,
                      onChanged: (value){
                           setState(() {
                           isSwitchedTutoring=value;
                           if(value==true){
                             categories.add(environment);
                           }
                           if(value==false){
                             categories.remove(environment);
                           }
                           print(isSwitchedEnvironment);
                           print(categories);
                           print(isSwitchedTutoring);
                      });
                      },
                      activeTrackColor: Colors.lightGreenAccent,
                      activeColor: Colors.green,
                        ),
                    ),
                  ]),
                RaisedButton(
                  color:  Colors.green,
                  child: Text(
                    'update',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async{
                    if(_formKey.currentState.validate()){
                      await DatabaseService(uid: user.uid).updateUserData(
                        _currentBirthDate ?? userData.birthDate,
                        _currentDescription ?? userData.description,
                        _currentFirstName ?? userData.firstName,
                        _currentLastName ?? userData.lastName,
                        _currentUserName ?? userData.username,
                        _currentPicture ?? userData.picture,
                        _activities ?? userData.activities,
                        _interested ?? userData.interested,
                        categories ?? userData.categories,
                        _reviews ?? userData.reviews,
                        );
                        Navigator.pop(context);
                    }
                  },
                )
              ],
            ),
             ]
            ),
            
            );
          }
          else{
            return Container(
              
            );
          }
          
        }
      ),
    );
  }
}