import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/models/activitie.dart';
import 'package:volunteering/services/http.dart';


class Map extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
final HttpService httpService = HttpService();
Completer<GoogleMapController> _controller = Completer();

void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
}

  Position position;
  Position activity;

  void initState(){
    super.initState();
    getCurrentLocation();
  }

  Future<Position> getCurrentLocation() async {
    Position currentLocation;
    try {
      currentLocation = await Geolocator().getCurrentPosition();
       setState(() {
      position=currentLocation;
    });
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  // getCurrentLocation() async{
  //  Position res = await Geolocator().getCurrentPosition();
  //  setState(() {
  //    position=res;
  //  });
 // }
  
  
  Marker _createMarker(){
    return 
      Marker(
        markerId: MarkerId('Home'),
        position: LatLng((position.latitude) , position.longitude),
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(title: "Home"),
      );
  }

 List<Marker> allMarkers = [];
// Add it to Set

  @override
  Widget build(BuildContext context) {

    

    var connectionStatus= Provider.of<ConnectivityStatus>(context);
    var enable = false;
    if(connectionStatus==ConnectivityStatus.WiFi || connectionStatus== ConnectivityStatus.Cellular ){
      enable = true;  
    }
      
    return Scaffold(
      appBar: AppBar(
        title: Text("Activities Location"),
        backgroundColor: Colors.green,
        ),
      body: Stack(children: [
        Container(
          child: FutureBuilder(
            future: Future.wait([httpService.getActivities(),getCurrentLocation()]),
            builder: (BuildContext context,
             AsyncSnapshot<List<dynamic>> snapshot) {
              if (enable==true){
              if (!snapshot.hasData || position.latitude==null || position.longitude==null) {
                    return Center(child: CircularProgressIndicator());
              }
              List<Activitie> actividades = snapshot.data[0];
              print(actividades);
              print('aca estoy');
              
              allMarkers = actividades.map((Activitie actividad) {
                print(actividad.location.longitude);
                print("3");
                return Marker(
                    markerId: MarkerId(actividad.name),
                    infoWindow: InfoWindow(title: actividad.name),
                    position: LatLng(actividad.location.latitude, actividad.location.longitude));
              }).toList();
              print("1");
              print(allMarkers);
              print("2");
              allMarkers.add(_createMarker());
              return GoogleMap(
                initialCameraPosition: CameraPosition(
                target: LatLng(40.7128, -74.0060)),
                markers: Set.from(allMarkers),
                onMapCreated: _onMapCreated,
              );
             }
             else{
               return AlertDialog(
                      title: Text("No Network available"),
                      content: Text("Your device is having connection issues. The map will be available when the connection is back"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Ok"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        )
                      ],
                    );
             }
            },
          ),
        ),
      ]),
    );
  }
        
}