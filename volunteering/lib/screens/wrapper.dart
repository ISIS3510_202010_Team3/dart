import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/screens/authenticate/authenticate.dart';
import 'package:volunteering/screens/home/home.dart';
import 'package:volunteering/models/user.dart';

class Wrapper extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    // retorna el home o la autenticacion widget
    if(user == null){
      return Authenticate();
    }else{
      return Home();
    }
  }
}