import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/auth.dart';
import 'package:path/path.dart';

class Register extends StatefulWidget {
 
 final Function toggleView;
 Register({this.toggleView});
 
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();

  //campo de texto
  String email='';
  String password='';
  String firstName='';
  String birthday='';
  String lastName='';
  String userName='';
  String description='';
  String error ='';
  String imagen='';
  String internet='';
  List<String> activities= new List();
  List<String> interested= new List();
  List<String> categories= new List();
  List<Review> reviews= new List<Review>();


  DateTime selectedDate = DateTime.now();




final date = TextEditingController();

  File image;
  String filename;

  Future _getImage() async{
    var selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      image=selectedImage;
      filename=basename(image.path);
    });
  }
 
  Future<String> uploadImage() async{
  StorageReference ref= FirebaseStorage.instance.ref().child(filename);
  StorageUploadTask uploadTask= ref.putFile(image);
  ref.putFile(image);

  var downUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
  var url = downUrl.toString();
  imagen=url;

  print("download URL : $url");

  return url;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2021));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        date.value = TextEditingValue(text: picked.toString().split(' ')[0]);
      });
  }

  @override
  Widget build(BuildContext context) {

    var connectionStatus= Provider.of<ConnectivityStatus>(context);
    var enable = false;

    if(connectionStatus== ConnectivityStatus.WiFi || connectionStatus== ConnectivityStatus.Cellular ){
      enable = true;  
    }
    else if(connectionStatus==ConnectivityStatus.Offline){
      setState(() => internet = 'You do not have internet connection');
    }

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          actions: <Widget>[
            FlatButton.icon(
              
              icon: Icon(Icons.cancel),
              label: Text('cancel',),
              onPressed: () {
                widget.toggleView();
              },
            ),
          ],
        ),  
        body: ListView(
          children: <Widget>[
          Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formkey,
            child: Column(
              children: <Widget>[
                Container(
                child:Align(
                alignment: Alignment.topLeft,
                  child: Text(
                    'Register form',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
                SizedBox(height: 4.0),
                Container(
                child:Align(
                alignment: Alignment.topLeft,
                  child: Text(
                    'Enter your basic information',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
              SizedBox(height: 20.0),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children:<Widget>[
                   Align(
                     alignment: Alignment.center,
                     child: CircleAvatar(
                       radius:100,
                       backgroundColor: Colors.blue,
                       child: ClipOval(
                         child: SizedBox(
                           width: 100.0,
                           height: 100.0,
                           child: (image!=null) ? Image.file(image,fit:BoxFit.fill)
                           :Image.network(
                             "https://image.shutterstock.com/image-vector/male-avatar-profile-picture-vector-260nw-149083895.jpg",
                             fit: BoxFit.fill,
                           ),
                         ),
                       ) ,
                     ),
                   ),
                   Padding(
                     padding:  EdgeInsets.only(top: 60.0),
                     child: IconButton(
                       icon: Icon(
                         Icons.camera,
                         size: 30.0,
                       ),
                       onPressed: (){
                         _getImage();
                       },
                     ),

                   )
                ],
              ),
              SizedBox(height: 20.0),
              RaisedButton(
                  color: Colors.green,
                  child: Text(
                    'Confirm Image',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: (enable==true?() async {
                   uploadImage();
                  }: null),
                ),
              SizedBox(height: 20.0),
               TextFormField(
                    style: TextStyle(fontSize: 20.0),
                    decoration: InputDecoration(
                      hintText: ' First name',
                      focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((25.0)),
                      borderSide: BorderSide(color: Colors.blue, width:2.0)
                      ),
                      enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.grey, width: 2.0))
                    ), 
                    validator: (val) => val.isEmpty ? 'Enter your first name' : null,
                    onChanged: (val) {
                      setState(() => firstName = val);                     
                    }
                  ),
                SizedBox(height: 5.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    hintText: 'Last name',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  validator: (val) => val.isEmpty ? 'Enter your last name' : null,
                    onChanged: (val) {
                      setState(() => lastName = val);                     
                    }
                ),
                 SizedBox(height: 5.0,),
                 GestureDetector(
                  onTap: () => _selectDate(context),
                  child: AbsorbPointer(
                    child: TextFormField(
                      onTap: () => _selectDate(context),
                      style: TextStyle(fontSize: 20.0),
                      controller: date,
                      keyboardType: TextInputType.datetime,
                      decoration: InputDecoration(
                        hintText: 'Date of Birth',
                        focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((25.0)),
                        borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0)),
                      ),
                      validator: (val) => val.isEmpty ? 'Enter a valid birth date' : null,
                      onChanged: (val) {
                      setState(() => birthday =val );                     
                    }
                    ),
                  ),
                ),
            SizedBox(height: 20.0,),
                 SizedBox(height: 4.0),
                Container(
                child:Align(
                alignment: Alignment.topLeft,
                  child: Text(
                    'Enter a valid username, email and password',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
              SizedBox(height: 10.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    hintText: 'Username',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  validator: (val) => val.isEmpty ? 'Enter a valid username' : null,
                      onChanged: (val) {
                      setState(() => userName = val);                     
                  }
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),                 
                    validator: (val) => val.isEmpty ? 'Enter an email' : null,
                    onChanged: (val) {
                      setState(() => email = val);                     
                    }
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  obscureText: true,
                  validator: (val) => val.length < 6 ? 'Enter a password longer than 6 characters' : null,
                  onChanged: (val){
                    setState(() => password = val); 
                  }
                ),
                SizedBox(height: 4.0),
                Container(
                child:Align(
                alignment: Alignment.topLeft,
                  child: Text(
                    'Enter your description',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
               SizedBox(height: 10.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    hintText: 'About you',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  validator: (val) => val.isEmpty ? 'Enter a description' : null,
                    onChanged: (val) {
                      setState(() => description = val);                     
                    }
                ),
                
                RaisedButton(
                  color: Colors.green,
                  child: Text(
                    'Register',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: (enable==true?() async {
                   if(_formkey.currentState.validate()){
                    dynamic result = await _auth.registerWithEmailAndPassword(email, password, firstName, lastName, date.text, userName, description,imagen, activities, interested, categories, reviews);
                    if(result == null){
                      setState(() => error = 'Please supply a valid email');
                    } 
                   }
                  }: null),
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0)
                ),
                Text(
                  internet,
                  style:TextStyle(color: Colors.red, fontSize: 14.0)
                )
              ],
            ),
          ),
        ),
       ]),   
    );
  }
}