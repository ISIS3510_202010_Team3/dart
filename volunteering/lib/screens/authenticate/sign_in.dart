import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/services/auth.dart';

class SignIn extends StatefulWidget {
 
 final Function toggleView;
 SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();

  
  //campo de texto
  String email='';
  String password='';
  String error ='';
  String internet='';


  @override
  Widget build(BuildContext context) {
    var connectionStatus= Provider.of<ConnectivityStatus>(context);
    var enable = false;

    if(connectionStatus== ConnectivityStatus.WiFi || connectionStatus== ConnectivityStatus.Cellular ){
      enable = true;
      setState(() => internet = ""); 
      
    }
    else if(connectionStatus==ConnectivityStatus.Offline){
      setState(() => internet = 'You do not have internet connection');
    }


    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text('Volunteering Hub'),
        ),  
        body: ListView(
          children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formkey,
            child: Column(
              children: <Widget>[
                Container(
                child:Align(
                alignment: Alignment.topCenter,
                  child: Text(
                    ' Volunteering Hub',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
              SizedBox(height: 20.0),
                Container(
                  width:450,
                  height:300,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("lib/assets/logo.jpg"),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    labelText: "Email",
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  validator: (val) => val.isEmpty ? 'Enter an email' : null,
                  onChanged: (val) {
                    setState(() => email = val);                     
                  }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    labelText: "Password",
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((25.0)),
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                     enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.grey, width: 2.0))
                  ),
                  obscureText: true,
                  validator: (val) => val.length < 6 ? 'Enter a password mayor de 6 caracteres' : null,
                  onChanged: (val){
                    setState(() => password = val); 
                  }
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.green,
                  child: Text(
                    'sign in',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: (enable==true?() async {
                     if (_formkey.currentState.validate()){
                       dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                       if(result == null){
                         setState(() => error = 'Could not sign in with those credentials');
                       }
                        
                     }
                  }: null) ,
                ),
                SizedBox(height: 12.0),
                RaisedButton(
                  color: Colors.red,
                  child: Text(
                    'Register with us',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: ()  {
                      widget.toggleView();
                  },
                ),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0)
                ),
                Text(
                  internet,
                  style:TextStyle(color: Colors.red, fontSize: 14.0)
                )
              ],
            ),
          ),
        ), 
          ]
        ),  
    );
  }
}