
class Activitie{
  final List<String> volunteers;
  final String id;
  final String name;
  final String foundation;
  final String description;
  final int volunteersNeed;
  final int volunteersAttending;
  final String date;
  final List<Imagen> images;
  final String category;
  final Location location;
  
  Activitie({ 
  this.id,
  this.volunteers,
  this.name,
  this.foundation,
  this.description,
   this.volunteersNeed,
  this.volunteersAttending,
  this.date, 
  this.images,
  this.category,
  this.location
  });
  
  factory Activitie.fromJson(Map<String, dynamic> json){
    var list= json['images'] as List;
    print(list.runtimeType);
    List<Imagen> imagesList=list.map((i)=> Imagen.fromJson(i)).toList();
    
    return Activitie(
      volunteers: json['volunteers'].cast<String>(),
      id: json['_id'],
      name: json['name'],
      foundation: json['foundation'],
      description: json['description'],
      volunteersNeed: json['volunteersNeeded'],
      volunteersAttending: json['volunteersAttending'],
      date: json['date'],
      images: imagesList,
      category: json['category'],
      location: Location.fromJson(json['location'])
    );
  }

  Map<String, dynamic> toJson(){
    return{
      "volunteers": volunteers,
      "_id": id,
      "name": name,
      "foundation": foundation,
      "description": description,
      "volunteersNeeded": volunteersNeed,
      "volunteersAttending": volunteersAttending,
      "date":date,
      "images":images,
      "category": category,
      "location": location
    };
  }
}
  class Location{
  final double latitude;
  final double longitude;
  
  Location({ 
  this.latitude,
  this.longitude,
  });

  factory Location.fromJson(Map<String, dynamic> json){
    return Location(
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }

  Map<String, dynamic> toJson(){
    return{
      "latitude": latitude,
      "longitude": longitude
    };
  }
}

class Imagen{
  final String id;
  final String fileName;

  Imagen({this.id, this.fileName});

  factory Imagen.fromJson(Map<String, dynamic> json){
    return Imagen(
      id: json['_id'],
      fileName: json['fileName'],
    );
  }
   Map<String, dynamic> toJson(){
    return{
      "_id": id,
      "fileName": fileName
    };
  }
}