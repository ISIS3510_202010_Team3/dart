class User {

  final String uid;

  User({ this.uid });
}

class UserData {

  final String uid;
  final String birthDate;
  final String description;
  final String firstName;
  final String lastName;
  final String username;
  final String picture;
  final List<String> activities;
  final List<String> interested;
  final List<String> categories;
  final List<dynamic> reviews;

  //UserData({ this.uid, this.birthDate,this.description,this.firstName,this.lastName,this.userName, this.picture});
  UserData({ this.uid, this.birthDate,this.description,this.firstName,this.lastName,this.username, this.picture, this.activities, this.interested, this.categories, this.reviews});

 
}

class Review{
  final String comment;
  final String foundation;
  final String score;

  Review({ this.comment, this.foundation, this.score});

  factory Review.fromJson(Map<String, dynamic> json){
    return Review(
      comment: json['comment'],
      foundation: json['foundation'],
      score: json['score'],
    );
  }

  Map<String, dynamic> toJson(){
    return{
      "comment": comment,
      "foundation": foundation,
      "score": score
    };
  }

}