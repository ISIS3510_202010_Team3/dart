import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/enums/connectivity_status.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/screens/wrapper.dart';
import 'package:volunteering/services/auth.dart';
import 'package:volunteering/services/connection.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<User>.value(
          value: AuthService().user),
        StreamProvider<ConnectivityStatus>(
          create: (BuildContext context) => ConnectivityService().connectionStatusController.stream, 
        )
      ],
      child: MaterialApp(
        home: Wrapper(),
            ),
        );
      }
  }
