Flutter version required:
Flutter 1.12.13+hotfix.9

Link to the APK:
https://drive.google.com/file/d/1bHbqf7WpQs0H7-LB8dEBYwOzqE3fkaW0/view?usp=sharing

Considerations:
1. When registering a new user, please add a picture and tap on confirm image in order to properly save it.
2. Information of a registered user you can use:
email: pati@gmail.com
password: patricia

Dependencies and environment from the pubspec.yaml file:
environment:
  sdk: ">=2.1.0 <3.0.0"

dependencies:
  flutter:
    sdk: flutter
  
  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^0.1.2
  firebase_auth: ^0.15.5+3
  firebase_storage: ^3.1.5
  image_picker: ^0.6.5+2
  cloud_firestore: ^0.13.4+2
  google_maps_flutter: ^0.5.26+2
  geolocator: ^5.3.1
  geocoder: ^0.2.1
  flutter_spinkit: ^4.1.2+1
  provider: ^4.0.4
  flutter_cache_manager: ^1.2.2
  connectivity: ^0.4.8+2
  http: ^0.12.0+4
  intl:
  

dev_dependencies:
  flutter_test:
    sdk: flutter

